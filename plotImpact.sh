#! /bin/bash

# higgs combine commands

datacard="Datacard_mA20_com_test.txt"
output_ws="Datacard_mA20_com_test.root"
prof_name="_mA20_comb_v1"
fggff_combine_int_model_opts="--algo grid -M MultiDimFit --alignEdges 1 -t -1 --saveSpecifiedNuis all --saveInactivePOI 1 --setParameterRanges r=0.5,5.0 --robustFit=1 --setRobustFitAlgo=Minuit2,Migrad --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --cminFallbackAlgo Minuit2,0:1 -m 20  --cminDefaultMinimizerStrategy 0"

text2workspace.py $datacard -o $output_ws -m 20 higgsMassRange=15,70 --optimize-simpdf-constraints cms

combineTool.py -d $output_ws $fggff_combine_int_model_opts -n $prof_name -P r --fastScan --floatOtherPOIs 1 --expectSignal 1
plot1DScan.py --POI r "higgsCombine$prof_name.MultiDimFit.mH20.root" -o "r_mA20_comb_v1" --main-label "Expected" --y-max 20

combineTool.py -M Impacts -d $output_ws -m 20 -n impacts_$prof_name --doInitialFit --robustFit 1 -t -1 --setParameterRanges r=0.5,5.0 --expectSignal 1  --setRobustFitAlgo=Minuit2,Migrad --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --cminFallbackAlgo Minuit2,0:1 --cminDefaultMinimizerStrategy 0 --freezeParameters MH
combineTool.py -M Impacts -d $output_ws -m 20 -n impacts_$prof_name --robustFit 1 --cminDefaultMinimizerStrategy 0 --freezeParameters MH -P r --doFits --expectSignal 1 --parallel 8
combineTool.py -M Impacts -d $output_ws -m 20 -n impacts_$prof_name --robustFit 1 --cminDefaultMinimizerStrategy 0 --freezeParameters MH -P r -o impacts_$prof_name.json
plotImpacts.py -i impacts_$prof_name.json -o impacts_$prof_name\_r --POI r
